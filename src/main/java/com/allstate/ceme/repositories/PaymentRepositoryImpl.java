package com.allstate.ceme.repositories;

import com.allstate.ceme.dto.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public class PaymentRepositoryImpl implements PaymentRepository {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public int rowCount() {
        return (int)mongoTemplate.count(new Query(),Payment.class);
    }

    @Override
    public Payment findById(int id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        return mongoTemplate.findOne(query, Payment.class);
    }

    @Override
    public List<Payment> findByType(String accountType) {
        Query query = new Query();
        query.addCriteria(Criteria.where("type").is(accountType));
        return mongoTemplate.find(query, Payment.class);
    }

    @Override
    public int save(Payment payment) {
        Payment savedPayment =  mongoTemplate.save(payment);
        return savedPayment.getId();
    }
}

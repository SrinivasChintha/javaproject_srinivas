package com.allstate.ceme;

import com.allstate.ceme.dto.Payment;
import com.allstate.ceme.repositories.PaymentRepository;
import com.allstate.ceme.service.PaymentService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.SimpleDateFormat;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class JavaSpringProjectApplicationTests {
    @Autowired
    private PaymentService paymentService;

    @MockBean
    private PaymentRepository paymentRepository;

    private Payment payment;
    private java.util.Date date;
    @BeforeEach
    void setUp() throws Exception{
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

        String dateInString = "11-11-2020";
        date = formatter.parse(dateInString);
        payment = new Payment(1,date, "Savings",1000, 12345);
    }

    @Test
    public void savePaymentTest(){
        when(paymentRepository.save(payment)).thenReturn(payment.getId());
        assertEquals(payment.getId(),paymentService.save(payment));
    }

    @Test
    public void getRecordCountTest(){
        int count = 2;
        when(paymentRepository.rowCount()).thenReturn(count);
        assertEquals(count, paymentService.rowCount());
    }

    @Test
    public void getPaymentsByAccountTypeTest(){
        String accountType = "Savings";
        when(paymentRepository.findByType(accountType))
                .thenReturn(Stream.of(payment)
                .collect(Collectors.toList()));
        assertEquals(payment, paymentService.findByType(accountType).get(0));

    }

}

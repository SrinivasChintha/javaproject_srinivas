package com.allstate.ceme.controller;

import com.allstate.ceme.dto.Payment;
import com.allstate.ceme.service.PaymentService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(PaymentController.class)
@AutoConfigureMockMvc(addFilters=false)
public class PaymentControllerITest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PaymentService paymentService;

    private java.util.Date date;

    @BeforeEach
    void setUp() throws Exception{
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

        String dateInString = "11-11-2020";
        date = formatter.parse(dateInString);
    }

    @Test
    public void getRecordCountShouldReturnFromService() throws Exception{
        when(paymentService.rowCount()).thenReturn(2);
        this.mockMvc.perform(get("/api/v1/payment/count")).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string("2"));
    }

    @Test
    public void getPaymentsByAccountTypeSavingsShouldReturnFromService() throws Exception{

        Payment p = new Payment(1,date, "Savings",1000, 12345);
        List<Payment> paymentList = Arrays.asList(p);

        given(paymentService.findByType("Savings")).willReturn(paymentList);

        mockMvc.perform(get("/api/v1/payment/findPaymentsByAccountType/Savings")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$",hasSize(1)))
                .andExpect(jsonPath("$[0].type",is(p.getType())));
    }

    @Test
    public void getPaymentsByAccountTypeCurrentShouldReturnFromService() throws Exception{

        Payment p = new Payment(1,date, "Current",1000, 12345);
        List<Payment> paymentList = Arrays.asList(p);

        given(paymentService.findByType("Current")).willReturn(paymentList);

        mockMvc.perform(get("/api/v1/payment/findPaymentsByAccountType/Current")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$",hasSize(1)))
                .andExpect(jsonPath("$[0].type",is(p.getType())));
    }

    @Test
    public void getPaymentByIdShouldReturnFromService() throws Exception{

        Payment p = new Payment(1,date, "Savings",1000, 12345);
        given(paymentService.findById(1)).willReturn(p);

        mockMvc.perform(get("/api/v1/payment/findPaymentById/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id",is(p.getId())));
    }

    @Test
    public void savePaymentShouldReturnFromService() throws Exception{

        Payment p = new Payment(1,date, "Savings",1000, 12345);
        given(paymentService.save(p)).willReturn(p.getId());

        String json= new ObjectMapper().writeValueAsString(p);

        mockMvc.perform(post("/api/v1/payment/save")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json)
                .characterEncoding("utf-8"))
                .andExpect(status().isOk());
    }


}

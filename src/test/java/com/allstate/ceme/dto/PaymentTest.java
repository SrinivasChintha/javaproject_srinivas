package com.allstate.ceme.dto;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PaymentTest {

    private Payment payment;
    private Date date;
    @BeforeEach
    void setUp() throws Exception{
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

        String dateInString = "11-11-2020";
        date = formatter.parse(dateInString);
        payment = new Payment(1,date, "Savings",1000, 12345);
    }

    @Test
    void IsPaymentObject_NOT_Null_TRUE(){
        Assertions.assertNotNull(payment);
    }

    @Test
    void getIdTest() {
        assertEquals(1, payment.getId());
    }

    @Test
    void getTypeTest(){
        assertEquals("Savings", payment.getType());
    }

    @Test
    void getAmountTest() {
        assertEquals(1000, payment.getAmount());
    }

    @Test
    void getCustomerIdTest() {
        assertEquals(12345, payment.getCustomerId());
    }

    @Test
    void getPaymentDateTest(){
        Assertions.assertSame(date,payment.getPaymentDate());
    }

    @Test
    void toStringTest(){
        String toStringMsg = "Payment(id=1, paymentDate=Wed Nov 11 00:00:00 UTC 2020, type=Savings, amount=1000.0, customerId=12345)";
        assertEquals(toStringMsg,payment.toString());
    }

}

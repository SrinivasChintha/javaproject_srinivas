package com.allstate.ceme.repositories;

import com.allstate.ceme.dto.Payment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.MockitoAnnotations.initMocks;

public class PaymentRepositoryTest {
    @Mock
    PaymentRepository paymentRepository;

    private Payment payment,payment1;
    private java.util.Date date;

    @BeforeEach
    void setUp() throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

        String dateInString = "11-11-2020";
        date = formatter.parse(dateInString);
        payment = new Payment(1, date, "Savings", 1000, 12345);
        payment1 = new Payment(1, date, "Savings", 2000, 67891);
        initMocks(this);
    }

    @Test
    void rowCount_Success() {
        doReturn(1).when(paymentRepository).rowCount();
        assertEquals(1, paymentRepository.rowCount());
    }

    @Test
    void rowCount_Failure() {
        doReturn(2).when(paymentRepository).rowCount();
        assertNotEquals(1, paymentRepository.rowCount());
    }

    @Test
    void save_Success() {
        doReturn(1).when(paymentRepository).save(payment);
        assertEquals(1, paymentRepository.save(payment));
    }

    @Test
    void save_Failure() {
        doReturn(1).when(paymentRepository).save(payment);
        assertNotEquals(2, paymentRepository.save(payment));
    }

    @Test
    void findById_Success(){
        doReturn(payment).when(paymentRepository).findById(1);
        assertEquals(payment,paymentRepository.findById(1));
    }

    @Test
    void findById_Failure(){
        doReturn(payment1).when(paymentRepository).findById(2);
        assertNotEquals(payment,paymentRepository.findById(2));
    }

    @Test
    void findByType_Success(){
        List<Payment> paymentList = new ArrayList<>(2);
        paymentList.add(payment);
        doReturn(paymentList).when(paymentRepository).findByType("Savings");
        assertEquals(paymentList,paymentRepository.findByType("Savings"));
    }

    @Test
    void findByType_Failure(){
        List<Payment> paymentList = new ArrayList<>(2);
        paymentList.add(payment);
        List<Payment> paymentList1 = new ArrayList<>(2);
        paymentList1.add(payment1);

        doReturn(paymentList).when(paymentRepository).findByType("Savings");
        assertNotEquals(paymentList1,paymentRepository.findByType("Savings"));
    }
}

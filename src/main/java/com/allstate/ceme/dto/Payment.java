package com.allstate.ceme.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.*;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@ToString
@Document
public class Payment {
    @Id @Getter @Setter @PositiveOrZero
    private int id;
    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="dd-MM-yyyy") @Getter @Setter
    private Date paymentDate;
    @NotEmpty
    @Getter @Setter
    private String type;
    @Getter @Setter
    private double amount;
    @PositiveOrZero
    @Getter @Setter
    private int customerId;
}

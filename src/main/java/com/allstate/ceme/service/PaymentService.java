package com.allstate.ceme.service;

import com.allstate.ceme.dto.Payment;

import java.util.List;

public interface PaymentService {
    int rowCount();

    List<Payment> findByType(String accountType);

    int save(Payment payment);

    Payment findById(int id);

}

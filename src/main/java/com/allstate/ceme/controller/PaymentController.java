package com.allstate.ceme.controller;

import com.allstate.ceme.dto.Payment;
import com.allstate.ceme.service.PaymentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/api/v1/payment")
public class PaymentController {

    private static final String CLASS_NAME = PaymentController.class.getName();

    @Autowired
    private PaymentService paymentService;

    private Logger logger = LoggerFactory.getLogger(PaymentController.class);

    @GetMapping(path = "/status")
    public String getPaymentServiceStatus(){
        final String METHOD_NAME = "getPaymentServiceStatus()";
        logger.info("Entered into "+CLASS_NAME+ " And method:"+ METHOD_NAME);
        return "Payment Service Is Up & Running !!";
    }

    @PostMapping(path = "/save")
    public int savePayment(@Valid @RequestBody Payment payment){
        final String METHOD_NAME = "savePayment()";
        logger.info("Entered into "+CLASS_NAME+ " And method:"+ METHOD_NAME);
        logger.info("Payment information going to save is:"+payment.toString());
        return paymentService.save(payment);
    }

    @GetMapping(path = "/count")
    public int getRecordCount(){
        final String METHOD_NAME = "getRecordCount()";
        logger.info("Entered into "+CLASS_NAME+ " And method:"+ METHOD_NAME);
        int count = paymentService.rowCount();
        logger.info("Number of payment records exist in database:"+ count);
        return count;
    }

    @GetMapping(path = "/findPaymentById/{id}")
    public Payment getPaymentById(@PathVariable("id") int id){
        final String METHOD_NAME = "getPaymentById()";
        logger.info("Entered into "+CLASS_NAME+ " And method:"+ METHOD_NAME);
        logger.info("User requested payment id is:"+id);
        Payment payment =  paymentService.findById(id);
        logger.info("Payment object is :"+payment);
        logger.info("Payment information is found in the database for id:"+id);

        return payment;
    }

    @GetMapping(path = "/findPaymentsByAccountType/{type}")
    public List<Payment> getPaymentsByAccountType(@PathVariable("type") String type){
        final String METHOD_NAME = "getPaymentsByAccountType()";
        logger.info("Entered into "+CLASS_NAME+ " And method:"+ METHOD_NAME);
        logger.info("User requested payment account type is:"+type);
        List<Payment> paymentList =  paymentService.findByType(type);
        if(paymentList.size()>0){
            logger.info("At least one record found in the database");
        }else{
            logger.info("Zero records returned from the database");
        }
        return paymentList;
    }
}

package com.allstate.ceme.service;

import com.allstate.ceme.dto.Payment;
import com.allstate.ceme.exception.PaymentNotFoundException;
import com.allstate.ceme.repositories.PaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class PaymentServiceImpl implements PaymentService {

    @Autowired
    private PaymentRepository paymentRepository;

    @Override
    public int rowCount() {
        return paymentRepository.rowCount();
    }

    @Override
    public List<Payment> findByType(String accountType) {
        if(StringUtils.isEmpty(accountType)){
            throw new PaymentNotFoundException("Payment record with account type: "+ accountType+ " is not found in the DB records");
        }else{
            return paymentRepository.findByType(accountType);
        }
    }

    @Override
    public int save(Payment payment) {
        return paymentRepository.save(payment);
    }

    @Override
    public Payment findById(int id) {
        if(id > 0){
            return paymentRepository.findById(id);
        }else{
            throw new PaymentNotFoundException("Payment record with id: "+ id+ " is not found in the DB records");
        }
    }

}

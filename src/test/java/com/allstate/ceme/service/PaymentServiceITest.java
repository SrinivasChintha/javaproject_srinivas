package com.allstate.ceme.service;

import com.allstate.ceme.dto.Payment;
import com.allstate.ceme.exception.PaymentNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.SimpleDateFormat;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@SpringBootTest
public class PaymentServiceITest {
    @Autowired
    PaymentService paymentService;

    private Payment payment;
    private java.util.Date date;

    @BeforeEach
    void setUp() throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

        String dateInString = "11-11-2020";
        date = formatter.parse(dateInString);
        payment = new Payment(1, date, "Savings", 1000, 12345);
    }

    @Test
    public void save_And_findById_Success() {
        int i = paymentService.save(payment);
        assertEquals(i, paymentService.findById(1).getId());
    }

    @Test
    public void save_And_findById_Failure() {
        int i = paymentService.save(payment);
        assertNotEquals(i, paymentService.findById(2).getId());
    }

    @Test
    public void rowCount_Failure() {
        assertNotEquals(5, paymentService.rowCount());
    }

    @Test
    public void rowCount_Success() {
        assertEquals(8, paymentService.rowCount());
    }

    @Test
    public void findById_Success() {
        assertEquals(payment.getId(), paymentService.findById(1).getId());
    }

    @Test
    public void findById_Failure() {
        assertNotEquals(payment.getId(), paymentService.findById(2).getId());
    }

    @Test
    public void findByType_Success() {
        List<Payment> paymentList = paymentService.findByType("Savings");
        assertEquals(payment.getType(), paymentList.get(0).getType());
    }

    @Test
    public void findByType_Failure() {
        List<Payment> paymentList = paymentService.findByType("Current");
        assertNotEquals(payment.getType(), paymentList.get(0).getType());
    }

}

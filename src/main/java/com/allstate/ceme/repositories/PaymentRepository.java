package com.allstate.ceme.repositories;

import com.allstate.ceme.dto.Payment;

import java.util.List;

public interface PaymentRepository {
    int rowCount();
    Payment findById(int id);
    List<Payment> findByType(String accountType);
    int save(Payment payment);
}
